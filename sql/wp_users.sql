CREATE TABLE `wp_users` (  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `user_login` varchar(60) NOT NULL DEFAULT '',  `user_pass` varchar(255) NOT NULL DEFAULT '',  `user_nicename` varchar(50) NOT NULL DEFAULT '',  `user_email` varchar(100) NOT NULL DEFAULT '',  `user_url` varchar(100) NOT NULL DEFAULT '',  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',  `user_activation_key` varchar(255) NOT NULL DEFAULT '',  `user_status` int(11) NOT NULL DEFAULT '0',  `display_name` varchar(250) NOT NULL DEFAULT '',  PRIMARY KEY (`ID`),  KEY `user_login_key` (`user_login`),  KEY `user_nicename` (`user_nicename`),  KEY `user_email` (`user_email`)) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `wp_users` VALUES('1', 'sahli', '$P$BJLlbkp2e1EjQ24gTgjh5w1aD7lPTx.', 'sahli', 'mohamedsahli20@gmail.com', '', '2016-08-30 10:10:07', '', '0', 'sahli');
INSERT INTO `wp_users` VALUES('8', 'oussama', '$P$B/y0.qmWhpfok9ftP9f/OjrUfMqhRV0', 'oussama', 'benzitaoussamaas@gmail.com', '', '2016-09-22 19:02:19', '', '0', 'oussama benzita');
/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
