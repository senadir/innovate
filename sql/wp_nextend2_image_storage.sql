CREATE TABLE `wp_nextend2_image_storage` (  `id` int(11) NOT NULL AUTO_INCREMENT,  `hash` varchar(32) NOT NULL,  `image` text NOT NULL,  `value` mediumtext NOT NULL,  PRIMARY KEY (`id`),  UNIQUE KEY `hash` (`hash`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `wp_nextend2_image_storage` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_nextend2_image_storage` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
