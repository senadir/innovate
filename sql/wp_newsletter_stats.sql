CREATE TABLE `wp_newsletter_stats` (  `id` int(11) NOT NULL AUTO_INCREMENT,  `user_id` int(11) NOT NULL DEFAULT '0',  `email_id` int(11) NOT NULL DEFAULT '0',  `link_id` int(11) NOT NULL DEFAULT '0',  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',  `anchor` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',  `ip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',  `country` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',  PRIMARY KEY (`id`),  KEY `email_id` (`email_id`),  KEY `user_id` (`user_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_newsletter_stats` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_newsletter_stats` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
